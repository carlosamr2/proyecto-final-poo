/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galeria2;

import entities.Imagen;
import entities.Album;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Carlos
 */
public class Galeria2 extends Application {
    
    @Override
    public void start(Stage stage) {
        HashMap<Album, ArrayList<Imagen>> albumes =new HashMap<>();
        ComboBox albumSelect=new ComboBox();
        
        
        
        HBox panel=new HBox();
        Button añadir=new Button("añadir imagen");
        Button crearAlbum=new Button("Crear album");
        
        crearAlbum.setOnAction((evento)->{
            Button newAlbumOk=new Button("OK");
            Stage newAlbum=new Stage();
            HBox newAlbumBox=new HBox();
            TextField newAlbumName=new TextField();
            newAlbumBox.getChildren().add(new Label("Ingrese el nombre del album: "));
            newAlbumBox.getChildren().add(newAlbumName);
            newAlbumBox.getChildren().add(newAlbumOk);
            newAlbumOk.setOnAction((newAlbumEvent)->{
                albumes.put(new Album(newAlbumName.getText()), new ArrayList<>());
                albumSelect.getItems().add(newAlbumName.getText());
                newAlbum.close();
            });
            Scene newAlbumScene=new Scene(newAlbumBox,350,50);
            newAlbum.setTitle("album nuevo");
            newAlbum.setScene(newAlbumScene);
            newAlbum.show();
        });
        
        
        añadir.setOnAction((value)->{
            Stage add=new Stage();
            GridPane addPane=new GridPane();
            Label addNombre=new Label("Nombre: ");
            Label addPersonas=new Label("Personas: ");
            Label addLugar=new Label("Lugar: ");
            Label addFecha=new Label("Fecha: ");
            Label addDescripcion=new Label("Descripcion: ");
            Label addImagen=new Label("Imagen: ");
            
            TextField newNombre=new TextField();
            TextField newPersonas=new TextField();
            TextField newLugar=new TextField();
            TextField newFecha=new TextField();
            TextField newDescripcion=new TextField();
            
            addPane.add(addNombre, 0, 0);
            addPane.add(addPersonas, 0, 1);
            addPane.add(addLugar, 0, 2);
            addPane.add(addFecha, 0, 3);
            addPane.add(addDescripcion, 0, 4);
            addPane.add(addImagen, 0, 5);
            
            addPane.add(newNombre, 1, 0);
            addPane.add(newPersonas, 1, 1);
            addPane.add(newLugar, 1, 2);
            addPane.add(newFecha, 1, 3);
            addPane.add(newDescripcion, 1, 4);
            
            Button examinar=new Button("Examinar");
            
            addPane.setHgap(10);
            addPane.setVgap(10);
            
            addPane.add(examinar,1,5);
            
            
            Scene addScene=new Scene(addPane,250,210);
            add.setScene(addScene);
            add.setTitle("Añadir imagen");
            add.show();
        });
        
        
        
        BorderPane root = new BorderPane(); 
        root.setTop(new Label("Galeria"));
        root.setBottom(panel);
        panel.getChildren().add(albumSelect);
        panel.getChildren().add(añadir);
        panel.getChildren().add(crearAlbum);
        ScrollPane desplazador=new ScrollPane();
        VBox caja=new VBox();
        TilePane tilePane = new TilePane();
        tilePane.setAlignment(Pos.CENTER);
        tilePane.setPadding(new Insets(15, 15, 15, 15));
        tilePane.setVgap(30);
        tilePane.setHgap(20);
        desplazador.setContent(tilePane);
        caja.getChildren().add(desplazador);
        root.setLeft(caja);
        albumDeMuestra(albumes,albumSelect,desplazador);
  
        Scene scene=new Scene(root,800,600);
        stage.setTitle("galeria");
        stage.setScene(scene);
        stage.show();
    }
    
    private void vistaPrevia(){
        ImageView miniatura=new ImageView();
        miniatura.setFitHeight(200);
        final FileChooser archivo=new FileChooser();
        File file=archivo.showOpenDialog(new Stage());
    
    }
    
    
    
    
    private void albumDeMuestra(HashMap albumes,ComboBox caja,ScrollPane desplazador){
        VBox vbox = new VBox();
        ArrayList<Imagen> muestras=new ArrayList<>();
        Album demo=new Album("Imagenes de muestra");
        Image imgLoader=new Image("file:carro.jpg");
        ImageView demoBig=new ImageView(imgLoader);
        ImageView demoSmall=new ImageView(imgLoader);
        Imagen carro=new Imagen("carro","NA","NA","NA","Vehiculo ultimo modelo");
        demoSmall.setFitWidth(150);
        demoSmall.setFitHeight(100);
        vbox.getChildren().add(demoSmall);
        caja.getItems().add(demo.getNombre());
        desplazador.setContent(vbox);
        caja.getSelectionModel().selectFirst();
        albumes.put(demo,carro);
        System.out.println(albumes.values());
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
    
}
